package dam.androidjavi.u2p4conversor;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class LogActivity extends AppCompatActivity {

    private static final String DEBUG_TAG = " LOG-";


    public void showInfoButton(String message){
        Log.i(DEBUG_TAG + super.getClass().getSimpleName(), message);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(DEBUG_TAG + super.getClass().getSimpleName(), "onCreate");

        notify("onCreate");
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (isFinishing()) {
            Log.i("isFinishing => ", "Cerrado por el usuario");
        } else {
            Log.i("isFinishing => ", "Cerrado por el sistema");
        }

        notify("onDestroy");

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Log.i(DEBUG_TAG + super.getClass().getSimpleName(), "Button Back is Pressed");

    }
// CICLOS DE VIDA

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onStart() {
        super.onStart();
        Log.i(DEBUG_TAG + super.getClass().getSimpleName(), "onStart");
        notify("onStart");

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onStop() {
        super.onStop();
        Log.i(DEBUG_TAG + super.getClass().getSimpleName(), "onStop");
        notify("onStop");

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onPause() {
        super.onPause();
        Log.i(DEBUG_TAG + super.getClass().getSimpleName(), "onPause");
        notify("onPause");

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onResume() {
        super.onResume();
        Log.i(DEBUG_TAG + super.getClass().getSimpleName(), "onResume");

        notify("onResume");

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(DEBUG_TAG + super.getClass().getSimpleName(), "onRestart");

        notify("onRestart");

    }



    //-------------
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void notify(String eventName) {
        String activityName = this.getClass().getSimpleName();

        String CHANNEL_ID = "My_LifeCycle";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, "My Lifecycle", NotificationManager.IMPORTANCE_DEFAULT);

            notificationChannel.setDescription("Lifecycle events");
            notificationChannel.setShowBadge(true);


            NotificationManager notificationManager = getSystemService(NotificationManager.class);

            if (notificationManager != null) {
                notificationManager.createNotificationChannel(notificationChannel);
            }

        }

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(eventName + " " + activityName)
                .setContentText(getPackageName())
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher);

        notificationManagerCompat.notify((int) System.currentTimeMillis(), notificationBuilder.build());
    }
}
