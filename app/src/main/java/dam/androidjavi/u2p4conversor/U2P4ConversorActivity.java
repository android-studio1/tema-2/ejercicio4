package dam.androidjavi.u2p4conversor;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import java.text.DecimalFormat;

public class U2P4ConversorActivity extends LogActivity {

    private TextView tError;
    private EditText etPulgada;
    private EditText etResultado;
    private Button buttonConvertir;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI() {
        //INITIALIZATION OF VARIABLES
        this.tError = findViewById(R.id.tError);
        this.etPulgada = findViewById(R.id.et_Pulgada);
        this.etResultado = findViewById(R.id.et_Resultado);
        this.buttonConvertir = findViewById(R.id.button_Convertir);

        buttonConvertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

                    String value = etPulgada.getText().toString();

                    if(value.equals("")){
                        throw new Exception("Inserta un valor.");
                    }else if ((Double.parseDouble(value) < 1)) {
                        throw new Exception("Sólo números >= 1.");
                    }

                    // TODO: I CHECK IF THE pToCm BUTTON IS PRESSED
                    RadioButton radioButton1 = (RadioButton) findViewById(R.id.pToCm);
                    etResultado.setText(convertir(value, radioButton1.isChecked()));

                    //TODO: BUTTON CLICKED
                    showInfoButton("Botón de conversión pulsado.");

                } catch (Exception e) {
                    Log.e("LogsConversor", e.getMessage());
                    showError(e.getMessage());
                }

            }
        });
    }

    private void showError(String message) {
        //TODO: SHOW THE ERROR IN THE VIEW
        this.tError.setText(message);
    }

    private String convertir(String value, boolean pToCm) {
        // TODO: ADD ANOTHER PARAMETER KNOW THE TYPE OF CONVERSION
        double res;

        if (pToCm) {
            res = Double.parseDouble(value) * 2.54;
        } else {
            res = Double.parseDouble(value) / 2.54;
        }

        this.tError.setText("");
        DecimalFormat precision = new DecimalFormat("0.00");
        return String.valueOf(precision.format(res));
    }


    //SAVE AND RESTORE DATA
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {

        //TODO: SAVE TEXT ERROR
        outState.putString("tError", this.tError.getText().toString());

        //TODO: WE SAVE FIELD DATA
        outState.putString("etPulgada", this.etPulgada.getText().toString());
        outState.putString("etResultado", this.etResultado.getText().toString());

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        //TODO: RESTORE TEXT ERROR
        this.tError.setText(savedInstanceState.getString("tError"));

        //TODO: WE RESTORE FIELD DATA
        this.etPulgada.setText(savedInstanceState.getString("etPulgada"));
        this.etResultado.setText(savedInstanceState.getString("etResultado"));

    }


}